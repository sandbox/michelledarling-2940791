<?php

namespace Drupal\forumfilter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ForumFilterForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'forumfilter_form';
  }
   /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('forumfilter.settings');

    $form['filter_words'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Words to filter'),
      '#default_value' => $config->get('forumfilter.filter_words'),
      '#description' => $this->t('Type one word or phrase per line. These words and phrases will trigger an email being sent to a monitor'),
    );

    $form['mod_email'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Moderator Email'),
      '#default_value' => $config->get('forumfilter.mod_email'),
      '#description' => $this->t('Email address where the email should be sent if filtered words or phrases are used'),
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('forumfilter.settings');
    $config->set('forumfilter.filter_words', $form_state->getValue('filter_words'));
    $config->set('forumfilter.mod_email', $form_state->getValue('mod_email'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'forumfilter.settings',
    ];
  }

}